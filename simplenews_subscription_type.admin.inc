<?php
// $Id$

/**
 * @file
 * Simplenews subscription type editing UI.
 */
 
/**
 * UI controller.
 */
class SimplenewsSubscriptionTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage simplenews subscriptions, including fields.';
    return $items;
  }
}

/**
 * Generates the simplenews subscription type editing form.
 */
function simplenews_subscription_type_form($form, &$form_state, $subscription_type, $op = 'edit') {

  if ('clone' === $op) {
    $subscription_type->label .= ' (cloned)';
    $subscription_type->type .= '_clone';
  }
  
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $subscription_type->label,
    '#description' => t('The human readable name of this simplenews subscription type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($subscription_type->type) ? $subscription_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $subscription_type->isLocked(),
    '#machine_name' => array(
      'exists' => 'simplenews_subscription_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this simplenews subscription type. It must contains lowercase letters, numbers, and underscores.'),
  );
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save simplenews subscription type'),
    '#weight' => 40,
  );
  
  if (!$subscription_type->isLocked() && $op != 'add') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete simplenews subscription type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('simplenews_subscription_type_form_submit_delete'),
    );
  }
  
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function simplenews_subscription_type_form_submit(&$form, &$form_state) {
  $subscription_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back
  $subscription_type->save();
  $form_state['redirect'] = 'admin/people/simplenews/types';
}

/**
 * Form API submit callback for the delete button.
 */
function simplenews_subscription_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = "admin/people/simplenews/types/manage/{$form_state['simplenews_subscription_type']->type}/delete";
}
