<?php
// $Id$

/**
 * Implements hook_entity_info().
 */
function simplenews_subscription_entity_info() {
  $return = array(
    'simplenews_subscription' => array(
      'label' => t('Newsletter subscription'),
      'entity class' => 'SimplenewsSubscription',
      'controller class' => 'EntityAPIController',
      'base table' => 'simplenews_subscription',
      'fieldable' => TRUE,
      'view modes' => array(
        'full' => array(
          'label' => t('Full'),
          'custom settings' => FALSE,
        ),
      ),
      'entity keys' => array(
        'id' => 'sid',
        'bundle' => 'type',
      ),
      'bundles' => array(),
      'bundle keys' => array(
        'bundle' => 'type',
      ),
      'uri callback' => 'simplenews_subscription_uri_callback',
      'access callback' => 'simplenews_subscription_access',
      'module' => 'simplenews_subscription',
      // 'metadata controller' => 'SimplenewsSubscriptionMetadataController',
      'admin ui' => array(
        'path' => 'admin/people/simplenews',
        'file' => 'simplenews_subscription.admin.inc',
        'controller class' => 'SimplenewsSubscriptionUIController',
      ),
    ),
    
    'simplenews_subscription_type' => array(
      'label' => t('Newsletter subscription type'),
      'entity class' => 'SimplenewsSubscriptionType',
      'controller class' => 'EntityAPIController',
      'base table' => 'simplenews_subscription_type',
      'fieldable' => FALSE,
      'bundle of' => 'simplenews_subscription',
      'exportable' => TRUE,
      'entity keys' => array(
        'id' => 'stid',
        'name' => 'type',
        'label' => 'label',
      ),
      'access callback' => 'simplenews_subscription_type_access',
      'module' => 'simplenews_subscription',
      'admin ui' => array(
        'path' => 'admin/people/simplenews/types',
        'file' => 'simplenews_subscription_type.admin.inc',
        'controller class' => 'SimplenewsSubscriptionTypeUIController',
      ),
    ),
  );
  
  return $return;
}

/**
 * Implements hook_entity_info_alter().
 */
function simplenews_subscription_entity_info_alter(&$entity_info) {
  foreach (simplenews_subscription_get_types() as $type => $info) {
    $entity_info['simplenews_subscription']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/people/simplenews/types/manage/%simplenews_subscription_type',
        'real path' => 'admin/people/simplenews/types/manage/' . $type,
        'bundle argument' => 5,
        'access arguments' => array('administer simplenews subscriptions'),
      ),
    );
  }
}

/**
 * Menu argument loader. Loads Simplenews subscription type by string.
 *
 * @param $type_name
 *   The machine readable name of simplenews subscription type to load.
 * @return
 *   A simplenews subscription type or FALSE if $type_name does not exist.
 */
function simplenews_subscription_type_load($type_name) {
  return simplenews_subscription_get_types($type_name);
}

/**
 * Implements hook_permission().
 */
function simplenews_subscription_permission() {
  $permissions = array();
  return $permissions;
}

/**
 * Gets an array of all simplenews subscription types, keyed by type name.
 *
 * @param $type_name
 *   If set, the type with the given name is returned.
 * @return SimplenewsSubscriptionType[]
 *   Depending whether $type_name is set, an array of simplenews subscription types or a single one.
 */
function simplenews_subscription_get_types($type_name = NULL) {
  $types = entity_load('simplenews_subscription_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}

/**
 * Fetch a simplenews subscription object.
 *
 * @param $sid
 *   Integer specifying the simplenews subscription id.
 * @param $reset
 *   A boolean indicating the internal cache should be reset.
 * @return
 *   A fully loaded $simplenews_subscription object or FALSE if cannot be loaded.
 *
 * @see simplenews_subscription_load_multiple()
 */
function simplenews_subscription_load($sid, $reset = FALSE) {
  $subscriptions = simplenews_subscription_load_multiple(array($sid), array(), $reset);
  return reset($subscriptions);
}

/**
 * Load multiple simplenews subscriptions based on certain conditions.
 *
 * @param $sids
 *   An array of simplenews subscription IDs.
 * @param $conditions
 *   An array of conditions match against the {simplenews_subscription} table.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   An array of simplenews subscription objects, indexed by sid.
 *
 * @see entity_load()
 * @see simplenews_subscription_load()
 */
function simplenews_subscription_load_muiltiple($sids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('simplenews_subscription', $sids, $conditions, $reset);
}

/* function simplenews_subscription_(delete|save) defined in simplenews.module */

/**
 * Delete multiple simplenews subscriptions.
 *
 * @param $sids
 *   An array of simplenews subscription IDs.
 */
function simplenews_subscription_delete_multiple(array $sids) {
  entity_get_controller('simplenews_subscription')->delete($sids);
}

/**
 * Create a new simplenews subscription object.
 */
function simplenews_subscription_create(array $values) {
  return new SimplenewsSubscription($values);
}

/**
 * Saves a simplenews subscription type to the db.
 *
 * @param $type
 *   The simplenews subscription type object.
 */
function simplenews_subscription_type_save(SimplenewsSubscriptionType $type) {
  $type->save();
}

/**
 * Deletes a simplenews subscription type from the db.
 *
 * @param $type
 *   The simplenews subscription type object.
 */
function simplenews_subscription_type_delete(SimplenewsSubscriptionType $type) {
  $type->delete();
}

function simplenews_subscription_uri_callback($subscription) {
  return array(
    'path' => "newsletter/{$subscription->tid}/subscription/{$subscription->snid}",
    // 'options' => array('fragment' => "subscription-{$subscription->type}"), // not yet required
  );
}

function simplenews_subscription_menu_alter(&$menu) {
  // Ezen az oldalon láthatóak a feliratkozók - ennek az oldalnak az új listára kell mutatnia
  // $menu['admin/people/simplenews']['page callback'] = 'simplenews_subscription_subscribers_list_page';
}

/**
 * The class used for simplenews subscriptions
 */
class SimplenewsSubscription extends EntityDB {

  public function __construct($values = array()) {
    parent::__construct($values, 'simplenews_subscription');
  }
  
  /**
   * Gets the associated simplenews subscription type object.
   *
   * @return SimplenewsSubscriptionType
   */
  public function getType() {
    return simplenews_subscription_get_types($this->type);
  }
  
  /**
   * Returns the URI for this simplenews subscription. May be altered via hook_entity_info().
   */
  public function uri() {
    return entity_uri('simplenews_subscription', $this);
  }
  
  /**
   * Returns the full url() for this simplenews subscription.
   */
  public function url() {
    $uri = $this->uri();
    return url($uri['path'], $uri);
  }
  
  /**
   * Returns the drupal path to this simplenews subscription.
   */
  public function path() {
    $uri = $this->uri();
    return $uri['path'];
  }
  
  public function buildContent($view_mode = 'full', $langcode = NULL) {
    $content = array();
    return entity_get_controller($this->entityType)->buildContent($this, $view_mode, $langcode, $content);
  }
}

/**
 * This is a separate class for simplenews subscription types.
 */
class SimplenewsSubscriptionType extends EntityDB {

  public function __construct($values = array()) {
    parent::__construct($values, 'simplenews_subscription_type');
  }
  
  /**
   * Returns whether the simplenews subscription type is locked, thus may not be deleted or renamed.
   *
   * Simplenews subscription types provided in code are automatically treated as locked as well as any fixed simplenews subscription type.
   */
  public function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}

function simplenews_subscription_access($op, $subscription = NULL, $account = NULL) {
  if (user_access('administer simplenews subscriptions', $account)) {
    return TRUE;
  }
  if (('create' === $op) && user_access('subscribe to newsletters', $account)) {
    return TRUE;
  }
}

function simplenews_subscription_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer simplenews subscriptions', $account);
}

/**
 * Implements hook_forms().
 */
function simplenews_subscription_forms() {
  $forms = array();
  if ($types = simplenews_subscription_get_types()) {
    foreach (array_keys($types) as $type) {
      $forms['simplenews_subscription_edit_' . $type . '_form']['callback'] = 'simplenews_subscription_edit_form';
    }
  }
  
  // overwrite default block forms
  foreach (simplenews_get_mailing_lists(TRUE) as $list) {
    $forms['simplenews_block_form_' . $list->tid] = array(
      'callback' => 'simplenews_subscription_block_form',
      'callback arguments' => array($list->tid),
    );
  }
  dsm($forms);
  return $forms;
}

/**
 * Generates the subscription form.
 */
function simplenews_subscription_edit_form($form, &$form_state, $subscription, $op = 'edit') {
  $form['#action'] = url("newsletter/subscribe/{$subscription->tid}/{$subscription->type}", array('absolute' => TRUE));
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['subscribe'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
    '#weight' => 50,
  );
  field_attach_form('simplenews_subscription', $subscription, $form, $form_state);
  dsm($form);
  return $form;
}

/**
 * Overrides default simplenews block forms
 */
function simplenews_subscription_block_form($form, &$form_state, $tid) {
  $subscription = entity_create('simplenews_subscription', array(
    'snid' => 0,
    'tid' => $tid,
    'status' => 0,
    'timestamp' => time(),
    'type' => 'default',
  ));
  return simplenews_subscription_edit_form($form, $form_state, $subscription);
}

/**
 * Implements hook_module_implements_alter().
 */
function simplenews_subscription_module_implements_alter(&$implementations, $hook) {
  if ('forms' === $hook) {
    dsm($implementations);
    // move our hook_forms() implementations to the end.
    $group = $implementations['simplenews_subscription'];
    unset($implementations['simplenews_subscription']);
    $implementations['simplenews_subscription'] = $group;
  }
}
