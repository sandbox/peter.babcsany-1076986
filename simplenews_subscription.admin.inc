<?php
// $Id$

/**
 * @file
 * Simplenews subscription type editing UI.
 */
 
/**
 * UI controller.
 */
class SimplenewsSubscriptionUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage simplenews subscriptions, including fields.';
    return $items;
  }
}

/**
 * Generates the simplenews subscription type editing form.
 */
function simplenews_subscription_form($form, &$form_state, $subscription, $op = 'edit') {

  if ('clone' === $op) {
    $subscription->label .= ' (cloned)';
    $subscription->type .= '_clone';
  }
  
  $form = simplenews_subscription_edit_form($form, $form_state, $subscription, $op);
    
  if (!$subscription_type->isLocked() && $op != 'add') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete simplenews subscription type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('simplenews_subscription_form_submit_delete'),
    );
  }
  
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function simplenews_subscription_type_form_submit(&$form, &$form_state) {
  $subscription = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back
  $subscription->save();
  $form_state['redirect'] = 'admin/people/simplenews';
}

/**
 * Form API submit callback for the delete button.
 */
function simplenews_subscription_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = "admin/people/simplenews/manage/{$form_state['simplenews_subscription']->sid}/delete";
}
